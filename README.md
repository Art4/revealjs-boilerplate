# reveal.js boilerplate

Build presentations with reveal.js with zip export, build with webpack.

## Requirements

- nodejs 6+

## How to use

1. Clone this repository
2. Install all dependencies with `npm install`
3. Do your changes in `index.html` and/or in `src/index.js`
4. Run `npm start` to run the deploy and automatically open the `index.html` in a browser

## Export

Run `npm run export` to export your presentation into a single zip file.
