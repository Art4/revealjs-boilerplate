import bar from './bar';
import Reveal from 'reveal.js';
import 'reveal.js/css/reveal.css';
import 'reveal.js/css/theme/white.css';

Reveal.initialize();

bar();
